package com.progresssoft.containers;

import com.progresssoft.Utils;
import com.progresssoft.entity.DealsFilesEntity;
import com.progresssoft.manager.ProgressSoftEntityManager;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

import static org.apache.commons.lang3.StringUtils.isNotBlank;


public class DealsFilesEntityContainer {

    public static List<DealsFilesEntity> getAllFiles(ProgressSoftEntityManager manager, String name) {
        CriteriaQuery<DealsFilesEntity> criteria = manager.getEntityManager().getCriteriaBuilder().createQuery(DealsFilesEntity.class);
        Root<DealsFilesEntity> root = criteria.from(DealsFilesEntity.class);
        criteria.select(root);
        if (isNotBlank(name))
            criteria.where(manager.getCriteriaBuilder().like(root.get("fileName"), Utils.buildLikeValue(name)));

        return manager.getEntityManager().createQuery(criteria).getResultList();


    }

}
