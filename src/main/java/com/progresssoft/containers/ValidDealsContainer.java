package com.progresssoft.containers;

import com.progresssoft.entity.ValidDealsEntity;
import com.progresssoft.manager.ProgressSoftEntityManager;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

import static com.progresssoft.Utils.checkLimit;
import static com.progresssoft.Utils.checkOffset;
import static org.apache.commons.lang3.StringUtils.isNotBlank;


public class ValidDealsContainer {

    public static List<ValidDealsEntity> getValidDeals(ProgressSoftEntityManager manager, String offset, String limit, String id) {


        CriteriaQuery<ValidDealsEntity> criteria = manager.getEntityManager().getCriteriaBuilder().createQuery(ValidDealsEntity.class);

        Root<ValidDealsEntity> root = criteria.from(ValidDealsEntity.class);
        criteria.select(root);
        if (isNotBlank(id))
            criteria.where(manager.getCriteriaBuilder().equal(root.get("fileId"), id));

        return manager.getEntityManager().createQuery(criteria)
                .setFirstResult(checkOffset(offset))
                .setMaxResults(checkLimit(limit)).getResultList();

    }

    public static List<ValidDealsEntity> getValidDealsTotal(ProgressSoftEntityManager manager, String offset, String limit, String id) {


        CriteriaQuery<ValidDealsEntity> criteria = manager.getEntityManager().getCriteriaBuilder().createQuery(ValidDealsEntity.class);

        Root<ValidDealsEntity> root = criteria.from(ValidDealsEntity.class);
        criteria.select(root);
        if (isNotBlank(id))
            criteria.where(manager.getCriteriaBuilder().equal(root.get("fileId"), id));

        return manager.getEntityManager().createQuery(criteria)
                .getResultList();

    }


}
