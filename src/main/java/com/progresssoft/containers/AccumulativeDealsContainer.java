package com.progresssoft.containers;

import com.progresssoft.Utils;
import com.progresssoft.entity.AccumulativeDealsEntity;
import com.progresssoft.manager.ProgressSoftEntityManager;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

import static org.apache.commons.lang3.StringUtils.isNotBlank;


public class AccumulativeDealsContainer {

    public static List<AccumulativeDealsEntity> getAccumulativeDeals(ProgressSoftEntityManager manager, String currency) {

        CriteriaQuery<AccumulativeDealsEntity> criteria = manager.getEntityManager().getCriteriaBuilder().createQuery(AccumulativeDealsEntity.class);

        Root<AccumulativeDealsEntity> root = criteria.from(AccumulativeDealsEntity.class);
        criteria.select(root);
        if (isNotBlank(currency))
            criteria.where(manager.getCriteriaBuilder().like(root.get("currencyCode"), Utils.buildLikeValue(currency)));

        return manager.getEntityManager().createQuery(criteria).getResultList();

    }

}
