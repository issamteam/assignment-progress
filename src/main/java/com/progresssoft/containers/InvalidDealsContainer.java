package com.progresssoft.containers;

import com.progresssoft.entity.InvalidDealsEntity;
import com.progresssoft.manager.ProgressSoftEntityManager;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

import static com.progresssoft.Utils.checkLimit;
import static com.progresssoft.Utils.checkOffset;
import static org.apache.commons.lang3.StringUtils.isNotBlank;


public class InvalidDealsContainer {

    public static List<InvalidDealsEntity> getInvalidDeals(ProgressSoftEntityManager manager, String offset, String limit, String id) {


        CriteriaQuery<InvalidDealsEntity> criteria = manager.getEntityManager().getCriteriaBuilder().createQuery(InvalidDealsEntity.class);

        Root<InvalidDealsEntity> root = criteria.from(InvalidDealsEntity.class);
        criteria.select(root);
        if (isNotBlank(id))
            criteria.where(manager.getCriteriaBuilder().equal(root.get("fileId"), id));

        return manager.getEntityManager().createQuery(criteria)
                .setFirstResult(checkOffset(offset))
                .setMaxResults(checkLimit(limit)).getResultList();

    }

    public static List<InvalidDealsEntity> getInvalidDealsTotal(ProgressSoftEntityManager manager, String offset, String limit, String id) {


        CriteriaQuery<InvalidDealsEntity> criteria = manager.getEntityManager().getCriteriaBuilder().createQuery(InvalidDealsEntity.class);

        Root<InvalidDealsEntity> root = criteria.from(InvalidDealsEntity.class);
        criteria.select(root);
        if (isNotBlank(id))
            criteria.where(manager.getCriteriaBuilder().equal(root.get("fileId"), id));

        return manager.getEntityManager().createQuery(criteria)

                .getResultList();

    }


}
