package com.progresssoft.listener;


import com.progresssoft.manager.ProgressSoftEntityManager;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;


public class InquiryListener implements ServletContextListener {
    private ProgressSoftEntityManager manager;

    public InquiryListener() {
    }


    public void contextInitialized(ServletContextEvent sce) {
        manager = new ProgressSoftEntityManager("assignment");
        sce.getServletContext().setAttribute("manager", manager);


    }

    public void contextDestroyed(ServletContextEvent sce) {
        if (manager != null) {
            manager.closeFactory();
            sce.getServletContext().setAttribute("manager", null);
        }
    }


}
