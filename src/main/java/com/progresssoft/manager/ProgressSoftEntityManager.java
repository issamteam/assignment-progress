package com.progresssoft.manager;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;


public class ProgressSoftEntityManager {

    private EntityManagerFactory emf = null;
    private EntityManager em;

    public ProgressSoftEntityManager(String name) {

        if (emf == null)
            emf = Persistence.createEntityManagerFactory(name);
        if (em == null)
            em = emf.createEntityManager();
    }

    public CriteriaBuilder getCriteriaBuilder() {
        return emf.getCriteriaBuilder();
    }

    public EntityManager getEntityManager() {
        return em;
    }

    public void closeFactory() {

        if (emf != null && emf.isOpen()) {
            emf.close();
        }
    }

}
