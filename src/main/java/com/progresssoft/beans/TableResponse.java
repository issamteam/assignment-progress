package com.progresssoft.beans;

import java.util.LinkedList;
import java.util.List;


public class TableResponse {

    private int draw;
    private int recordsTotal;
    private int recordsFiltered;
    private List<? extends Object> data;

    public TableResponse(String draw, List<? extends Object> data) {
        if (data == null) data = new LinkedList<>();
        this.data = data;
        this.draw = Integer.parseInt(draw);
        this.recordsTotal = data.size();
        this.recordsFiltered = data.size();
    }


    public TableResponse(String draw, List<? extends Object> data, int size) {
        if (data == null) data = new LinkedList<>();
        this.data = data;
        this.draw = Integer.parseInt(draw);
        this.recordsTotal = size;
        this.recordsFiltered = size;
    }


    public int getDraw() {
        return draw;
    }

    public void setDraw(int draw) {
        this.draw = draw;
    }

    public int getRecordsTotal() {
        return recordsTotal;
    }

    public void setRecordsTotal(int recordsTotal) {
        this.recordsTotal = recordsTotal;
    }

    public int getRecordsFiltered() {
        return recordsFiltered;
    }

    public void setRecordsFiltered(int recordsFiltered) {
        this.recordsFiltered = recordsFiltered;
    }

    public List<? extends Object> getData() {
        return data;
    }

    public void setData(List<? extends Object> data) {
        this.data = data;
    }
}
