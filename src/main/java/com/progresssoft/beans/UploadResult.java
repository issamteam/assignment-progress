package com.progresssoft.beans;

import com.progresssoft.Utils;


public class UploadResult {

    private String fileName;

    private long fileId;

    private long startTime;

    private long endTime;

    private long duration;

    private String durationText;

    private long totalRecords;

    private long totalValidRecords;

    private long totalInvalidRecords;

    private long totalValidInsertedRecords;

    private long totalInvalidInsertedRecords;

    private int errorMessage;

    public UploadResult() {
    }

    public long getFileId() {
        return fileId;
    }

    public void setFileId(long fileId) {
        this.fileId = fileId;
    }

    public int getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(int errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
        setDurationText(Utils.formatDuration(duration));
    }

    public String getDurationText() {
        return durationText;
    }

    public void setDurationText(String durationText) {
        this.durationText = durationText;
    }

    public long getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(long totalRecords) {
        this.totalRecords = totalRecords;
    }

    public long getTotalValidRecords() {
        return totalValidRecords;
    }

    public void setTotalValidRecords(long totalValidRecords) {
        this.totalValidRecords = totalValidRecords;
    }

    public long getTotalInvalidRecords() {
        return totalInvalidRecords;
    }

    public void setTotalInvalidRecords(long totalInvalidRecords) {
        this.totalInvalidRecords = totalInvalidRecords;
    }

    public long getTotalValidInsertedRecords() {
        return totalValidInsertedRecords;
    }

    public void setTotalValidInsertedRecords(long totalValidInsertedRecords) {
        this.totalValidInsertedRecords = totalValidInsertedRecords;
    }

    public long getTotalInvalidInsertedRecords() {
        return totalInvalidInsertedRecords;
    }

    public void setTotalInvalidInsertedRecords(long totalInvalidInsertedRecords) {
        this.totalInvalidInsertedRecords = totalInvalidInsertedRecords;
    }

    public void incrementValidInsertedRecords(int length) {
        totalValidInsertedRecords += length;
    }

    public void incrementInvalidInsertedRecords(int length) {
        totalInvalidInsertedRecords += length;
    }

    @Override
    public String toString() {
        return "UploadResult{" +
                "fileName='" + fileName + '\'' +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", duration=" + duration +
                ", durationText='" + durationText + '\'' +
                ", totalRecords=" + totalRecords +
                ", totalValidRecords=" + totalValidRecords +
                ", totalInvalidRecords=" + totalInvalidRecords +
                ", totalValidInsertedRecords=" + totalValidInsertedRecords +
                ", totalInvalidInsertedRecords=" + totalInvalidInsertedRecords +
                ", errorMessage=" + errorMessage +
                '}';
    }
}
