package com.progresssoft.beans;

import static org.apache.commons.lang3.StringUtils.isNotBlank;


public class DealsDetails {

    private String uniqueId;
    private String fromCurrency;
    private String toCurrency;
    private long dealDate;
    private double amount;
    private boolean valid;

    public DealsDetails() {
    }

    public DealsDetails(String uniqueId, String fromCurrency, String toCurrency, String dealDate, String amount) {
        this.uniqueId = uniqueId;
        this.fromCurrency = fromCurrency;
        this.toCurrency = toCurrency;
        this.dealDate = checkDate(dealDate);
        this.amount = checkAmount(amount);
        this.valid = checkIfValid();
    }

    private Long checkDate(String dealDate) {
        long timestamp = 0L;


        try {
            timestamp = Long.valueOf(dealDate);
        } catch (Exception e) {

        }

        return timestamp;
    }

    private double checkAmount(String amount) {
        double checkedAmount = 0;
        try {
            checkedAmount = Double.parseDouble(amount);

        } catch (Exception e) {

        }

        return checkedAmount;
    }

    private boolean checkIfValid() {

        return (amount > 0 && isNotBlank(uniqueId) && isNotBlank(fromCurrency) && isNotBlank(toCurrency) && dealDate > 0);
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getFromCurrency() {
        return fromCurrency;
    }

    public void setFromCurrency(String fromCurrency) {
        this.fromCurrency = fromCurrency;
    }

    public String getToCurrency() {
        return toCurrency;
    }

    public void setToCurrency(String toCurrency) {
        this.toCurrency = toCurrency;
    }

    public long getDealDate() {
        return dealDate;
    }

    public void setDealDate(long dealDate) {
        this.dealDate = dealDate;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }
}
