package com.progresssoft;

import com.google.gson.Gson;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

import static org.apache.commons.lang3.StringUtils.isBlank;

public class Utils {

    public static final Gson gson = new Gson();
    private static final int DEFAULT_LIMIT = 50;
    private static final int DEFAULT_OFFSET = 0;

    public static String formatDuration(long duration) {
        Period period = new Period(duration);
        PeriodFormatter fmt = new PeriodFormatterBuilder()
                .printZeroAlways()
                .minimumPrintedDigits(2)
                .appendHours()
                .appendSeparator(":")
                .printZeroAlways()
                .minimumPrintedDigits(2)
                .appendMinutes()
                .appendSeparator(":")
                .printZeroAlways()
                .minimumPrintedDigits(2)
                .appendSeconds()
                .appendSeparator(".")
                .printZeroAlways()
                .minimumPrintedDigits(3)
                .appendMillis3Digit()
                .toFormatter();
        return fmt.print(period);
    }

    public static String formatDateTime(DateTime date) {

        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        return fmt.print(date);

    }

    public static String buildLikeValue(String value) {
        return "%" + value.toLowerCase() + "%";
    }

    public static int checkOffset(String offset) {
        if (isBlank(offset)) return DEFAULT_OFFSET;
        try {
            return Integer.parseInt(offset);
        } catch (Exception e) {

            return DEFAULT_OFFSET;
        }
    }

    public static int checkLimit(String limit) {
        if (isBlank(limit)) return DEFAULT_LIMIT;
        try {
            return Integer.parseInt(limit);
        } catch (Exception e) {

            return DEFAULT_LIMIT;
        }
    }

}
