package com.progresssoft.utils;

import com.progresssoft.beans.DealsDetails;
import com.progresssoft.beans.UploadResult;
import com.progresssoft.models.DBHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;


public class CSVReader {

    private static final Logger logger = LoggerFactory.getLogger(CSVReader.class);

    private final static String COMMA = ",";

    private final HashSet<String> uniqueIds;
    private Function<String, DealsDetails> mapToItem = (line) -> {

        String[] array = line.split(COMMA);

        return validateForUniqueId(
                new DealsDetails(
                        validateUndefined(array, 0),
                        validateUndefined(array, 1),
                        validateUndefined(array, 2),
                        validateUndefined(array, 3),
                        validateUndefined(array, 4)));


    };

    public CSVReader() {

        uniqueIds = new HashSet<>(DBHelper.getUniqueIds());
    }

    public UploadResult processInputFile(String inputFilePath, UploadResult result) {


        List<DealsDetails> inputList = new ArrayList<DealsDetails>();

        try {

            File inputF = new File(inputFilePath);

            InputStream inputFS = new FileInputStream(inputF);

            BufferedReader br = new BufferedReader(new InputStreamReader(inputFS));

            // skip the header of the csv

            inputList = br.lines().skip(1).map(mapToItem).collect(Collectors.toList());

            br.close();
            result = filterData(inputList, result);

        } catch (Exception e) {

            logger.error(e.getMessage());
            result.setErrorMessage(9);


        }

        result.setTotalRecords(inputList.size());
        return result;

    }

    private String validateUndefined(String[] array, int index) {
        try {

            return array[index];
        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }

    private DealsDetails validateForUniqueId(DealsDetails dealsDetails) {

        if (uniqueIds.contains(dealsDetails.getUniqueId()))
            dealsDetails.setValid(false);
        else
            uniqueIds.add(dealsDetails.getUniqueId());

        return dealsDetails;

    }

    private UploadResult filterData(List<DealsDetails> items, UploadResult result) {

        List<DealsDetails> validItems = new LinkedList<>();
        List<DealsDetails> inValidItems = new LinkedList<>();

        validItems = items.stream().filter(new Predicate<DealsDetails>() {
            @Override
            public boolean test(DealsDetails searchedItem) {
                return searchedItem.isValid();
            }
        }).collect(Collectors.toCollection(LinkedList::new));


        inValidItems = items.stream().filter(new Predicate<DealsDetails>() {
            @Override
            public boolean test(DealsDetails searchedItem) {
                return !searchedItem.isValid();
            }
        }).collect(Collectors.toCollection(LinkedList::new));


        result.setTotalValidRecords(validItems.size());
        result.setTotalInvalidRecords(inValidItems.size());


        result = DBHelper.insertRecords(result, validItems, true);
            result = DBHelper.insertRecords(result, inValidItems, false);


        return result;
    }


}
