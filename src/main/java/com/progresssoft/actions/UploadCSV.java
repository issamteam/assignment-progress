package com.progresssoft.actions;

import com.progresssoft.beans.UploadResult;
import com.progresssoft.models.DBHelper;
import com.progresssoft.utils.CSVReader;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import static com.progresssoft.Utils.gson;
import static org.apache.commons.lang3.StringUtils.isBlank;


public class UploadCSV extends HttpServlet {

    private static final int THRESHOLD_SIZE = 1024 * 1024 * 30; // 30MB
    private static final int MAX_FILE_SIZE = 1024 * 1024 * 50; // 50MB
    private static final int REQUEST_SIZE = 1024 * 1024 * 50; // 50MB
    private static final Logger logger = LoggerFactory.getLogger(UploadCSV.class);

    private String uploadPath;

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        doPost(request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        long startTime = System.currentTimeMillis();

        String basePath = request.getServletContext().getInitParameter("file_path");

        UploadResult result = new UploadResult();
        result.setStartTime(startTime);

        this.uploadPath = basePath + "csv";


        File path = new File(this.uploadPath);
        if (!path.exists()) {

            boolean created = path.mkdir();
            if (!created) {
                logger.error("UploadCSV init param 'uploadPath' value '"
                        + this.uploadPath + "' cannot be created");
                result.setErrorMessage(8);
            }
        }

        if (!path.isDirectory()) {
            logger.error("UploadCSV init param 'uploadPath' value '"
                    + this.uploadPath + "' is actually not a directory in file system.");
            result.setErrorMessage(8);
        } else if (!path.canRead()) {
            logger.error("UploadCSV init param 'uploadPath' value '"
                    + this.uploadPath + "' is actually not readable in file system.");
            result.setErrorMessage(8);
        }

        if (result.getErrorMessage() != 8) {

            try {
                if (ServletFileUpload.isMultipartContent(request)) {


                    DiskFileItemFactory factory = new DiskFileItemFactory();

                    factory.setSizeThreshold(THRESHOLD_SIZE);


                    if (!new File("/opt/temp/").exists())
                        new File("/opt/temp/").mkdir();
                    factory.setRepository(new File("/opt/temp/"));


                    ServletFileUpload upload = new ServletFileUpload(factory);


                    upload.setHeaderEncoding("UTF-8");


                    upload.setFileSizeMax(MAX_FILE_SIZE);
                    upload.setSizeMax(REQUEST_SIZE);


                    try {

                        List fileItems = upload.parseRequest(request);

                        Iterator i = fileItems.iterator();

                        while (i.hasNext()) {
                            FileItem fi = (FileItem) i.next();
                            if (!fi.isFormField()) {

                                String fileName = fi.getName();
                                if (fi != null) {

                                    fileName = FilenameUtils.getName(fileName);


                                }

                                long sizeInBytes = fi.getSize();


                                File file = new File(uploadPath + "/" + fileName);
                                String extension = file.getName().substring(file.getName().lastIndexOf(".") + 1, file.getName().length());

                                result.setFileName(fileName);
                                if (isBlank(extension) || !extension.toLowerCase().equalsIgnoreCase("csv")) {
                                    result.setErrorMessage(7);
                                } else {

                                    if (sizeInBytes <= MAX_FILE_SIZE) {

                                        long fileId = DBHelper.insertFile(result.getFileName());
                                        if (fileId <= 0) result.setErrorMessage(10);
                                        else {
                                            result.setFileId(fileId);

                                            boolean exists = DBHelper.checkIfFileNameExistsInDB(fileName);
                                            if (!exists) {

                                                result.setErrorMessage(2);
                                                fi.write(file);
                                                CSVReader reader = new CSVReader();
                                                result = reader.processInputFile(file.getAbsolutePath(), result);
                                            } else
                                                result.setErrorMessage(4);
                                        }
                                    } else {

                                        result.setErrorMessage(3);
                                    }


                                }
                            }
                        }


                    } catch (Exception ex) {

                        logger.error(ex.getMessage());

                        if (ex.getClass().toString().trim().contains("org.apache.commons.fileupload.FileUploadBase$SizeLimitExceededException")) {
                            result.setErrorMessage(3);
                        } else {
                            result.setErrorMessage(5);
                        }

                    }


                } else {
                    result.setErrorMessage(5);
                }


            } catch (Exception e) {
                result.setErrorMessage(5);
                logger.error(e.getMessage());
            }
        }

        long endTime = System.currentTimeMillis();

        result.setEndTime(endTime);
        result.setDuration(endTime - startTime);

        if (result.getErrorMessage() == 2) {
            DBHelper.updateAccumulativeDeals();

        }

        DBHelper.updateFile(result);

        String json = null;
        json = gson.toJson(result);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
    }
}
