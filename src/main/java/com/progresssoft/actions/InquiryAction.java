package com.progresssoft.actions;

import com.progresssoft.beans.TableResponse;
import com.progresssoft.containers.AccumulativeDealsContainer;
import com.progresssoft.containers.DealsFilesEntityContainer;
import com.progresssoft.containers.InvalidDealsContainer;
import com.progresssoft.containers.ValidDealsContainer;
import com.progresssoft.entity.AccumulativeDealsEntity;
import com.progresssoft.entity.DealsFilesEntity;
import com.progresssoft.entity.InvalidDealsEntity;
import com.progresssoft.entity.ValidDealsEntity;
import com.progresssoft.manager.ProgressSoftEntityManager;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.progresssoft.Utils.gson;
import static org.apache.commons.lang3.StringUtils.isNotBlank;


public class InquiryAction extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String p = request.getParameter("p");
        if (isNotBlank(p)) {
            int process = 0;

            try {
                process = Integer.parseInt(p);
            } catch (Exception e) {

            }

            if (process > 0) {

                switch (process) {

                    case 1:
                        getFiles(request, response);
                        break;
                    case 2:
                        getAccumulativeDeals(request, response);

                        break;
                    case 3:
                        getValidDeals(request, response);

                        break;

                    case 4:
                        getInvalidDeals(request, response);

                        break;
                    default:
                        break;
                }


            }

        }
    }

    private void getValidDeals(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String draw = request.getParameter("draw");
        String size = request.getParameter("length");
        String offset = request.getParameter("start");
        String id = request.getParameter("id");
        List<ValidDealsEntity> validDeals = ValidDealsContainer.getValidDeals(((ProgressSoftEntityManager) getServletContext().getAttribute("manager")), offset, size, id);

        String json = null;
        json = gson.toJson(new TableResponse(draw, validDeals, ValidDealsContainer.getValidDealsTotal(((ProgressSoftEntityManager) getServletContext().getAttribute("manager")), offset, size, id).size()));

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);

    }

    private void getInvalidDeals(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String id = request.getParameter("id");
        String draw = request.getParameter("draw");
        String size = request.getParameter("length");
        String offset = request.getParameter("start");
        List<InvalidDealsEntity> invalidDeals = InvalidDealsContainer.getInvalidDeals(((ProgressSoftEntityManager) getServletContext().getAttribute("manager")), offset, size, id);

        String json = null;
        json = gson.toJson(new TableResponse(draw, invalidDeals, InvalidDealsContainer.getInvalidDealsTotal(((ProgressSoftEntityManager) getServletContext().getAttribute("manager")), offset, size, id).size()));
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);

    }

    private void getFiles(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String name = request.getParameter("name");
        List<DealsFilesEntity> files = DealsFilesEntityContainer.getAllFiles(((ProgressSoftEntityManager) getServletContext().getAttribute("manager")), name);
        String draw = request.getParameter("draw");

        String json = null;
        json = gson.toJson(new TableResponse(draw, files));
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
    }

    private void getAccumulativeDeals(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String currency = request.getParameter("currency");
        List<AccumulativeDealsEntity> accumulativeDeals = AccumulativeDealsContainer.getAccumulativeDeals(((ProgressSoftEntityManager) getServletContext().getAttribute("manager")), currency);
        String draw = request.getParameter("draw");

        String json = null;
        json = gson.toJson(new TableResponse(draw, accumulativeDeals));
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        doGet(request, response);
    }
}
