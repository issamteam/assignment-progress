package com.progresssoft.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "valid_deals", schema = "assignment")
public class ValidDealsEntity implements Serializable {
    private int id;
    private int fileId;
    private String dealUniqueId;
    private String fromCurrency;
    private String toCurrency;
    private String dealTimestamp;
    private String dealAmount;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "file_id", nullable = false)
    public int getFileId() {
        return fileId;
    }

    public void setFileId(int fileId) {
        this.fileId = fileId;
    }

    @Basic
    @Column(name = "deal_unique_id", nullable = true, length = 255)
    public String getDealUniqueId() {
        return dealUniqueId;
    }

    public void setDealUniqueId(String dealUniqueId) {
        this.dealUniqueId = dealUniqueId;
    }

    @Basic
    @Column(name = "from_currency", nullable = true, length = 255)
    public String getFromCurrency() {
        return fromCurrency;
    }

    public void setFromCurrency(String fromCurrency) {
        this.fromCurrency = fromCurrency;
    }

    @Basic
    @Column(name = "to_currency", nullable = true, length = 255)
    public String getToCurrency() {
        return toCurrency;
    }

    public void setToCurrency(String toCurrency) {
        this.toCurrency = toCurrency;
    }

    @Basic
    @Column(name = "deal_timestamp", nullable = true, length = 255)
    public String getDealTimestamp() {
        return dealTimestamp;
    }

    public void setDealTimestamp(String dealTimestamp) {
        this.dealTimestamp = dealTimestamp;
    }

    @Basic
    @Column(name = "deal_amount", nullable = true, length = 255)
    public String getDealAmount() {
        return dealAmount;
    }

    public void setDealAmount(String dealAmount) {
        this.dealAmount = dealAmount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ValidDealsEntity that = (ValidDealsEntity) o;

        if (id != that.id) return false;
        if (fileId != that.fileId) return false;
        if (dealUniqueId != null ? !dealUniqueId.equals(that.dealUniqueId) : that.dealUniqueId != null) return false;
        if (fromCurrency != null ? !fromCurrency.equals(that.fromCurrency) : that.fromCurrency != null) return false;
        if (toCurrency != null ? !toCurrency.equals(that.toCurrency) : that.toCurrency != null) return false;
        if (dealTimestamp != null ? !dealTimestamp.equals(that.dealTimestamp) : that.dealTimestamp != null)
            return false;
        if (dealAmount != null ? !dealAmount.equals(that.dealAmount) : that.dealAmount != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + fileId;
        result = 31 * result + (dealUniqueId != null ? dealUniqueId.hashCode() : 0);
        result = 31 * result + (fromCurrency != null ? fromCurrency.hashCode() : 0);
        result = 31 * result + (toCurrency != null ? toCurrency.hashCode() : 0);
        result = 31 * result + (dealTimestamp != null ? dealTimestamp.hashCode() : 0);
        result = 31 * result + (dealAmount != null ? dealAmount.hashCode() : 0);
        return result;
    }
}
