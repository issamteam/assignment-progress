package com.progresssoft.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "deals_files", schema = "assignment")
public class DealsFilesEntity implements Serializable {
    private int id;
    private String fileName;
    private Timestamp uploadDate;
    private Integer numberOfValid;
    private Integer numberOfInvalid;
    private Integer numberOfInsertedValid;
    private Integer numberOfInsertedInvalid;
    private Integer duration;
    private String durationText;
    private Integer errorNumber;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "file_name", nullable = false, length = 255)
    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Basic
    @Column(name = "upload_date", nullable = true)
    public Timestamp getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Timestamp uploadDate) {
        this.uploadDate = uploadDate;
    }

    @Basic
    @Column(name = "number_of_valid", nullable = true)
    public Integer getNumberOfValid() {
        return numberOfValid;
    }

    public void setNumberOfValid(Integer numberOfValid) {
        this.numberOfValid = numberOfValid;
    }

    @Basic
    @Column(name = "number_of_invalid", nullable = true)
    public Integer getNumberOfInvalid() {
        return numberOfInvalid;
    }

    public void setNumberOfInvalid(Integer numberOfInvalid) {
        this.numberOfInvalid = numberOfInvalid;
    }

    @Basic
    @Column(name = "number_of_inserted_valid", nullable = true)
    public Integer getNumberOfInsertedValid() {
        return numberOfInsertedValid;
    }

    public void setNumberOfInsertedValid(Integer numberOfInsertedValid) {
        this.numberOfInsertedValid = numberOfInsertedValid;
    }

    @Basic
    @Column(name = "number_of_inserted_invalid", nullable = true)
    public Integer getNumberOfInsertedInvalid() {
        return numberOfInsertedInvalid;
    }

    public void setNumberOfInsertedInvalid(Integer numberOfInsertedInvalid) {
        this.numberOfInsertedInvalid = numberOfInsertedInvalid;
    }

    @Basic
    @Column(name = "duration", nullable = true)
    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    @Basic
    @Column(name = "duration_text", nullable = true, length = 255)
    public String getDurationText() {
        return durationText;
    }

    public void setDurationText(String durationText) {
        this.durationText = durationText;
    }

    @Basic
    @Column(name = "error_number", nullable = true)
    public Integer getErrorNumber() {
        return errorNumber;
    }

    public void setErrorNumber(Integer errorNumber) {
        this.errorNumber = errorNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DealsFilesEntity that = (DealsFilesEntity) o;

        if (id != that.id) return false;
        if (!fileName.equals(that.fileName)) return false;
        if (uploadDate != null ? !uploadDate.equals(that.uploadDate) : that.uploadDate != null) return false;
        if (numberOfValid != null ? !numberOfValid.equals(that.numberOfValid) : that.numberOfValid != null)
            return false;
        if (numberOfInvalid != null ? !numberOfInvalid.equals(that.numberOfInvalid) : that.numberOfInvalid != null)
            return false;
        if (numberOfInsertedValid != null ? !numberOfInsertedValid.equals(that.numberOfInsertedValid) : that.numberOfInsertedValid != null)
            return false;
        if (numberOfInsertedInvalid != null ? !numberOfInsertedInvalid.equals(that.numberOfInsertedInvalid) : that.numberOfInsertedInvalid != null)
            return false;
        if (duration != null ? !duration.equals(that.duration) : that.duration != null) return false;
        if (durationText != null ? !durationText.equals(that.durationText) : that.durationText != null) return false;
        return errorNumber != null ? errorNumber.equals(that.errorNumber) : that.errorNumber == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + fileName.hashCode();
        result = 31 * result + (uploadDate != null ? uploadDate.hashCode() : 0);
        result = 31 * result + (numberOfValid != null ? numberOfValid.hashCode() : 0);
        result = 31 * result + (numberOfInvalid != null ? numberOfInvalid.hashCode() : 0);
        result = 31 * result + (numberOfInsertedValid != null ? numberOfInsertedValid.hashCode() : 0);
        result = 31 * result + (numberOfInsertedInvalid != null ? numberOfInsertedInvalid.hashCode() : 0);
        result = 31 * result + (duration != null ? duration.hashCode() : 0);
        result = 31 * result + (durationText != null ? durationText.hashCode() : 0);
        result = 31 * result + (errorNumber != null ? errorNumber.hashCode() : 0);
        return result;
    }


}
