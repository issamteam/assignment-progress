package com.progresssoft.entity;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "accumulative_deals", schema = "assignment")
public class AccumulativeDealsEntity implements Serializable {
    private int id;
    private String currencyCode;
    private Integer dealsCount;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "currency_code", nullable = true, length = 255)
    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    @Basic
    @Column(name = "deals_count", nullable = true)
    public Integer getDealsCount() {
        return dealsCount;
    }

    public void setDealsCount(Integer dealsCount) {
        this.dealsCount = dealsCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccumulativeDealsEntity that = (AccumulativeDealsEntity) o;

        if (id != that.id) return false;
        if (currencyCode != null ? !currencyCode.equals(that.currencyCode) : that.currencyCode != null) return false;
        if (dealsCount != null ? !dealsCount.equals(that.dealsCount) : that.dealsCount != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (currencyCode != null ? currencyCode.hashCode() : 0);
        result = 31 * result + (dealsCount != null ? dealsCount.hashCode() : 0);
        return result;
    }
}
