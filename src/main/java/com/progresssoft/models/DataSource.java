package com.progresssoft.models;

import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.SQLException;

import static com.progresssoft.models.DBConfig.*;
import static java.util.concurrent.TimeUnit.SECONDS;


public class DataSource {

    private static final int VALID_TIMEOUT = 4;
    private static final long CONNECTION_TIMEOUT = SECONDS.toMillis(8);

    private static DataSource instance;

    private static HikariDataSource dataSource;

    private DataSource() {
        buildDataSource();

    }

    public static DataSource getInstance() {

        if (instance == null) {
            instance = new DataSource();
        }
        return instance;
    }

    public void buildDataSource() {


        dataSource = new HikariDataSource();

        dataSource.setMaximumPoolSize(50);
        dataSource.setMinimumIdle(2);
        dataSource.setDriverClassName(DRIVER_CLASS);
        dataSource.setUsername(DB_USER);
        dataSource.setPassword(DB_PWD);
        dataSource.setJdbcUrl(DB_URL);

        dataSource.addDataSourceProperty("cachePrepStmts", "true");
        dataSource.addDataSourceProperty("prepStmtCacheSize", "250");
        dataSource.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
        dataSource.addDataSourceProperty("useServerPrepStmts", "true");
        dataSource.setConnectionTimeout(CONNECTION_TIMEOUT);


    }

    public HikariDataSource getDataSource() throws SQLException {
        if (dataSource == null)

            getInstance();
        return dataSource;
    }

    public void checkConnection() throws SQLException {

        Connection connection = getDataSource().getConnection();
        connection.isValid(VALID_TIMEOUT);
        connection.close();
    }

    public Connection getConnection() throws SQLException {
        return getDataSource().getConnection();
    }

    public void beginTransaction(Connection connection) throws SQLException {
        connection.setAutoCommit(false);
    }

    public void endTransaction(Connection connection) throws SQLException {
        connection.commit();
        setAutoCommitTrue(connection);
        connection.close();
    }

    public void rollback(Connection connection) throws SQLException {
        connection.rollback();
    }

    public void setAutoCommitTrue(Connection connection) throws SQLException {
        connection.setAutoCommit(true);
    }
}

