package com.progresssoft.models;

import com.progresssoft.beans.DealsDetails;
import com.progresssoft.beans.UploadResult;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.ColumnListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;

import static com.progresssoft.Utils.formatDateTime;


public class DBHelper {

    private static final Logger logger = LoggerFactory.getLogger(DBHelper.class);
    private static final int BATCH_SIZE = 1000;
    private static final String VALID_DEALS_TABLE = "valid_deals";
    private static final String INVALID_DEALS_TABLE = "invalid_deals";
    private static final String CHECK_IF_FILE_EXIST = "SELECT id FROM deals_files WHERE lower(file_name)=?";
    private static final String INSERT_DEAL = "INSERT INTO `assignment`.%s (`file_id`,`deal_unique_id`,`from_currency`,`to_currency`,`deal_timestamp`,`deal_amount`)VALUES(?,?,?,?,?,?)";
    private static final String INSERT_FILE = "INSERT INTO `assignment`.`deals_files`(`file_name`,`upload_date`)VALUES(?,?)";
    private static final String GET_ALL_UNIQUE_IDS = "SELECT deal_unique_id FROM valid_deals";
    private static final String DELETE_ALL_ACCUMULATIVE_DEALS = "DELETE FROM accumulative_deals";
    private static final String UPDATE_ACCUMULATIVE_DEALS = "insert into accumulative_deals (currency_code,deals_count) SELECT from_currency,count(*) as dealsCount FROM assignment.valid_deals group by from_currency";
    private static final String UPDATE_FILE_INFO = "UPDATE assignment.deals_files SET number_of_valid =?,number_of_invalid = ?,number_of_inserted_valid = ?,number_of_inserted_invalid = ?,duration = ?,duration_text = ?,error_number = ? WHERE id = ?";

    public static boolean checkIfFileNameExistsInDB(String fileName) {

        ResultSetHandler<Integer> idHandler = new ScalarHandler<>();

        try {
            QueryRunner queryRunner = new QueryRunner(DataSource.getInstance().getDataSource());

            Integer id = queryRunner.query(CHECK_IF_FILE_EXIST, idHandler, fileName.toLowerCase());
            return id != null && id > 0;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
            return false;
        }
    }

    public static long insertFile(String fileName) {
        QueryRunner queryRunner = null;


        try {

            queryRunner = new QueryRunner(DataSource.getInstance().getDataSource());
            ResultSetHandler<Long> idHandler = new ScalarHandler<>();
            Long result = queryRunner.insert(INSERT_FILE, idHandler, fileName, formatDateTime(new DateTime()));

            if (result == null)
                return 0L;


            return result;


        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
            return 0L;
        }
    }

    public static UploadResult insertRecords(UploadResult oldResult, List<DealsDetails> deals, boolean valid) {

        QueryRunner queryRunner = null;


        try {
            queryRunner = new QueryRunner(DataSource.getInstance().getDataSource());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
            return oldResult;
        }


        String sql = String.format(INSERT_DEAL, VALID_DEALS_TABLE);
        if (!valid) sql = String.format(INSERT_DEAL, INVALID_DEALS_TABLE);
        int i = 0;
        int size = BATCH_SIZE;
        if (deals.size() < size) size = deals.size();
        Object[][] values = new Object[size][6];

        int inserted = 0;

        for (DealsDetails deal : deals) {
            try {


                Object[] dealValues = {oldResult.getFileId(), deal.getUniqueId(), deal.getFromCurrency(), deal.getToCurrency(), deal.getDealDate(), deal.getAmount()};

                values[i++] = dealValues;


                if (i % size == 0) {

                    int[] result = queryRunner.batch(sql, values);


                    if (result != null && result.length > 0) {

                        if (valid) {

                            oldResult.incrementValidInsertedRecords(result.length);

                        } else oldResult.incrementInvalidInsertedRecords(result.length);
                    }
                    inserted += size;
                    if (deals.size() - inserted < size)
                        size = deals.size() - inserted;
                    values = new Object[size][6];
                    i = 0;


                }

            } catch (Exception e) {
                e.printStackTrace();
                logger.error(e.getMessage());


            }


        }

        return oldResult;

    }

    public static List<String> getUniqueIds() {

        try {
            QueryRunner queryRunner = new QueryRunner(DataSource.getInstance().getDataSource());
            ResultSetHandler<List<String>> beanListHandler = new ColumnListHandler<>();

            return queryRunner.query(GET_ALL_UNIQUE_IDS, beanListHandler);
        } catch (Exception e) {

            logger.error(e.getMessage());
            return new LinkedList<>();
        }


    }

    public static void updateAccumulativeDeals() {
        try {
            QueryRunner queryRunner = new QueryRunner(DataSource.getInstance().getDataSource());

            queryRunner.update(DELETE_ALL_ACCUMULATIVE_DEALS);
            queryRunner.update(UPDATE_ACCUMULATIVE_DEALS);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
    }

    public static void updateFile(UploadResult result) {
        try {
            QueryRunner queryRunner = new QueryRunner(DataSource.getInstance().getDataSource());

            queryRunner.update(UPDATE_FILE_INFO, result.getTotalValidRecords(), result.getTotalInvalidRecords(), result.getTotalValidInsertedRecords(), result.getTotalInvalidInsertedRecords(), result.getDuration(), result.getDurationText(), result.getErrorMessage(), result.getFileId());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
    }
}
