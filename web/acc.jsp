<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Progress Soft- Accumulative Deals</title>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"/>
    <script src="https://code.jquery.com/jquery-1.9.1.js"></script>
    <link rel="stylesheet" href="assets/css/style.css" media="screen">

    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="assets/js/acc.js"></script>
</head>

<body>
<table>
    <tr>
        <td style="width: 200px!important;">
            <div class="menu">

                <ul style="list-style: none;width: 200px;">
                    <li><a href="index.jsp"> Upload</a></li>
                    <li><a href="files.jsp"> Inquiry</a></li>
                    <li><a href="acc.jsp"> Accumulative Deals</a>
                    </li>
                </ul>

            </div>
        </td>
        <td style="width: 80%;">
            <table id="acc" class="display" style="width:100%">
    <thead>
    <tr>
        <th>Currency</th>
        <th>Deals Count</th>


    </tr>
    </thead>

            </table>
        </td>
    </tr>
</table>
</body>
</html>