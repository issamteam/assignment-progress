<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Progress Soft- invalid Deals</title>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"/>
    <script src="https://code.jquery.com/jquery-1.9.1.js"></script>
    <link rel="stylesheet" href="assets/css/style.css" media="screen">

    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="assets/js/invalid.js"></script>
</head>

<script>

    $(document).ready(function () {
        getData(<%=request.getParameter("id")%>)
    });
</script>
<body>
<table>
    <tr>
        <td style="width: 200px!important;position:fixed!important;">
            <div class="menu">

                <ul style="list-style: none;width: 200px;">
                    <li><a href="index.jsp"> Upload</a></li>
                    <li><a href="files.jsp"> Inquiry</a></li>
                    <li><a href="acc.jsp"> Accumulative Deals</a>
                    </li>
                </ul>

            </div>
        </td>
        <td style="width: 80%;">
            <table id="invalid" class="display" style="width:100%">
    <thead>
    <tr>
        <th>Deal Unique Id</th>
        <th>From Currency</th>
        <th>To Currency</th>
        <th>Deal Timestamp</th>
        <th>Deal Amount</th>

    </tr>
    </thead>

            </table>
        </td>
    </tr>
</table>
</body>
</html>