$(document).ready(function () {
    $('#files').DataTable({
        "processing": true,
        "serverSide": true,
        "dom": 'Bfrtip',
        "pageLength": 50,
        "searching": false,
        "ajax": {"url": "/inquiry?p=1", "dataSrc": "data"},
        "columns": [
            {
                "data": "fileName"
            },
            {
                "data": "uploadDate"
            },
            {
                "data": "numberOfValid"
            },
            {
                "data": "numberOfInvalid"
            },
            {
                "data": "numberOfInsertedValid"
            },
            {
                "data": "numberOfInsertedInvalid"
            },
            {
                "data": "durationText"
            },
            {
                "data": "errorNumber", render: function (data, type, row) {
                    return getErrorMessage(data);
                }
            },
            {
                "data": "validdata", render: function (data, type, row) {
                    var res = '-';
                    if (toInt(row.errorNumber) === 2) {
                        res = '<form action="valid.jsp" method="post">' +
                            '<input type="hidden" name="id"value="' + row.id + '">' +
                            '<input type="submit" value="Show">' +
                            '</form>'
                    }

                    return res;
                }
            },
            {
                "data": "invaliddata", render: function (data, type, row) {
                    var res = '-';
                    if (toInt(row.errorNumber) === 2) {
                        res = '<form action="invalid.jsp" method="post">' +
                            '<input type="hidden" name="id" value="' + row.id + '">' +
                            '<input type="submit" value="Show">' +
                            '</form>'
                    }

                    return res;
                }
            }]
    });
});

function toInt(n) {
    return Math.ceil(Number(n));
}

function getErrorMessage(error) {

    var msg = toInt(error);
    var errorMessage = '';
    switch (msg) {
        case 0:
            errorMessage = 'Please try again later';
            break;
        case 2:
            errorMessage = 'No Error';
            break;
        case 3:
            errorMessage = 'Size Exceeded , Maximum 50 MB';
            break;
        case 4:
            errorMessage = 'File Already imported';
            break;
        case 5:
            errorMessage = 'Please try again later';
            break;
        case 6:
            errorMessage = 'Please try again later';
            break;
        case 7:
            errorMessage = 'Invalid Extension , Allowed CSV Only';
            break;
        case 8:
            errorMessage = 'Error in uploadPath ,maybe isn\'t an directory, unreadable or cannot be created please see logs';
            break;
        case 9:
            errorMessage = 'Error in parsing';
            break;
        case 10:
            errorMessage = 'Error in inserting in DB';
            break;
        default:
            errorMessage = 'Please try again later';
            break;
    }

    return errorMessage;
}