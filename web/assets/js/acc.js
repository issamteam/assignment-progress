$(document).ready(function () {
    $('#acc').DataTable({
        "processing": true,
        "serverSide": true,
        "dom": 'Bfrtip',
        "pageLength": 50,
        "searching": false,
        "ajax": {"url": "/inquiry?p=2", "dataSrc": "data"},
        "columns": [
            {
                "data": "currencyCode"
            },
            {
                "data": "dealsCount"
            }]
    });
});

