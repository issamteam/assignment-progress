function getData(id) {


    $('#valid').DataTable({
        "processing": true,
        "serverSide": true,
        "dom": 'Bfrtip',
        "pageLength": 50,
        "searching": false,
        "ajax": {"url": "/inquiry?p=3&id=" + id, "dataSrc": "data"},
        "columns": [
            {
                "data": "dealUniqueId"
            },
            {
                "data": "fromCurrency"
            },
            {
                "data": "toCurrency"
            },
            {
                "data": "dealTimestamp"
            },
            {
                "data": "dealAmount"
            }]
    });

}
