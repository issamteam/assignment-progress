<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Progress Soft</title>
    <link rel="stylesheet" href="assets/css/style.css" media="screen">

    <!-- Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script src="assets/js/plupload.full.js"></script>
    <script src="assets/js/jquery-progressbar.min.js"></script>
    <script src="assets/js/up.js"></script>

</head>

<body>
<table>
    <tr>
        <td style="width: 400px!important;">
            <div class="menu">

                <ul style="list-style: none;width: 400px;">
                    <li><a href="index.jsp"> Upload</a></li>
                    <li><a href="files.jsp"> Inquiry</a></li>
                    <li><a href="acc.jsp"> Accumulative Deals</a>
                    </li>
                </ul>

            </div>
        </td>
        <td style="width: 80%;">
            <div class="upload-form" id="uploader">

                <!-- Form Heading -->
                <h1>Upload CSV</h1>


                <!-- Select & Upload Button -->
                <div>
                    <a class="button" id="pickfiles" href="#">Select</a>
                    <a class="button" id="uploadfiles" href="#">Upload</a>
                </div>

                <!-- File List -->
                <div id="filelist" class="cb"></div>

                <!-- Progress Bar -->
                <div id="progressbar"></div>

                <!-- Close After Upload -->
                <div id="message">

                </div>

            </div>
        </td>
    </tr>
</table>
</body>
</html>